package msgEnc.msg;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.StringJoiner;

@Entity
@Table(name = "message")
@NamedQuery(name = "getMsgByHash", query = "from Message where hash =:msgByHash")

public class Message {

    @Id
    private String hash;
    private String msg;
    private String passToDecrypt;

    public Message() {
    }

    public Message(String hash, String msg, String passToDecrypt) {
        this.hash = hash;
        this.msg = msg;
        this.passToDecrypt = passToDecrypt;
    }


    public String getHash() {
        return hash;
    }

    public String getPassToDecrypt() {
        return passToDecrypt;
    }
    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Message.class.getSimpleName() + "[", "]")
                .add("hash='" + hash + "'")
                .add("msg='" + msg + "'")
                .add("passToDecrypt=" + passToDecrypt)
                .toString();
    }
}
