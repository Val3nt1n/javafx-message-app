package msgEnc.db;

import msgEnc.exceptions.MessageNotFoundException;
import msgEnc.msg.Message;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;


import java.security.Key;
import java.util.List;

public class DbOperations {

    public final static SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

    public static Message getMsgByHash(String msgByHash) throws MessageNotFoundException {
        assert DbOperations.sessionFactory != null;
        try (Session session = DbOperations.sessionFactory.openSession()) {
            Query<Message> queryMessages = session.createNamedQuery("getMsgByHash", Message.class);
            queryMessages.setParameter("msgByHash", msgByHash);
            List<Message> messagesList = queryMessages.list();
            if (messagesList.size() == 0)
                throw new MessageNotFoundException("Message " + msgByHash + " does not exist!");
            return messagesList.get(0);
        } catch (Exception e) {
            System.err.println("Something has happened..." + e);
            throw new MessageNotFoundException(e.getMessage());
        }
    }

    public static void newMessage(String hash, String msg, String pass) {
        Transaction transaction = null;
        assert sessionFactory != null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(new Message(hash, msg, pass));
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) transaction.rollback();
        }

    }


    public static void deleteMsg(Message message) {
        Transaction transaction = null;

        try {
            assert sessionFactory != null;
            Session session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.delete(message);
            transaction.commit();
            System.out.println("Deleted: " + message);
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) transaction.rollback();
        }

    }

}

