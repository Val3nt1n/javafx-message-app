package msgEnc.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import msgEnc.db.DbOperations;
import msgEnc.exceptions.MessageNotFoundException;
import msgEnc.msg.Message;
import net.bytebuddy.utility.RandomString;
import org.mindrot.jbcrypt.BCrypt;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private JFXButton newMsg;
    @FXML
    private PasswordField password;
    @FXML
    private TextArea textArea;
    @FXML
    private TextField msgIdent;
    @FXML
    private JFXButton encBtn, decBtn, loadBtn, closeBtn;

    private final int msgSize = 10;

    static String getAlphaNumericString(int n)
    {

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

    private static SecretKey mySecretDesKey;

    // Generate Des SecretKey
    public static void secretDesKey(String myDesKey) {
        try {
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("DES");
            byte[] dkeyBytes = myDesKey.getBytes();
            DESKeySpec desKeySpec = new DESKeySpec(dkeyBytes);
            mySecretDesKey = secretKeyFactory.generateSecret(desKeySpec);

        } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

    }

    public String encrypt(ActionEvent actionEvent) {
        if (textArea.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Text Area it's empty!");
            alert.showAndWait();
        } else {
            if (password.getText().length() < 8) {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Secret key need to be more then 10 chars");
                alert.showAndWait();
            } else {
                try {
                    String msgIdentifier = getAlphaNumericString(msgSize);
                    String generatedSecuredPasswordHash = BCrypt.hashpw(password.getText(), BCrypt.gensalt(12));
                    secretDesKey(password.getText());
                    Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
                    cipher.init(Cipher.ENCRYPT_MODE, mySecretDesKey);
                    DbOperations.newMessage(msgIdentifier, Base64.getEncoder().encodeToString(cipher.doFinal(textArea.getText().getBytes(StandardCharsets.UTF_8))), generatedSecuredPasswordHash);
                    textArea.clear();
                    password.clear();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION, "To idenitfy your message save this : " + msgIdentifier);
                    alert.show();
                    msgIdent.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public String decrypt(ActionEvent actionEvent) throws MessageNotFoundException {
        Message messageFromDb = DbOperations.getMsgByHash(msgIdent.getText());
        if (BCrypt.checkpw(password.getText(), messageFromDb.getPassToDecrypt())) {
            try {
                secretDesKey(password.getText());
                Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
                cipher.init(Cipher.DECRYPT_MODE, mySecretDesKey);
                byte[] dec = Base64.getDecoder().decode(messageFromDb.getMsg().getBytes(StandardCharsets.UTF_8));
                //Decrypt
                byte[] utf8 = cipher.doFinal(dec);
                //Decode using utf-8
                textArea.setText(new String(utf8, StandardCharsets.UTF_8));
                DbOperations.deleteMsg(messageFromDb);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Incorrect password to decrypt");
            alert.showAndWait();
        }
        return null;
    }

    public void newMsg(ActionEvent actionEvent){
        decBtn.setDisable(true);
        msgIdent.setVisible(false);
        textArea.clear();
        msgIdent.clear();
        password.clear();
        encBtn.setVisible(true);
        newMsg.setVisible(false);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        decBtn.setDisable(true);
        newMsg.setVisible(false);
        msgIdent.setVisible(false);
    }

    public void loadMsg(ActionEvent actionEvent) {
        encBtn.setVisible(false);
        decBtn.setDisable(false);
        msgIdent.setVisible(true);
        msgIdent.setDisable(false);
        newMsg.setVisible(true);

    }

    // Close app Action
    public void closeAction(ActionEvent actionEvent) {
        System.exit(0);
    }
}
