package msgEnc.exceptions;

public class MessageNotFoundException extends Exception{

    private String message;
    public MessageNotFoundException(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
